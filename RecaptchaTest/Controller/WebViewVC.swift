//
//  WebViewVC.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 2/11/2564 BE.
//

import UIKit
import WebKit

class WebViewVC: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var webView: WKWebView!
    let strUrl = "https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(sharedFile))
        loadfile()
    }
    
    func loadfile() {
        let url: URL! = URL(string: strUrl)
        webView.load(URLRequest(url: url))
    }
    
    @objc func sharedFile() {
//        let fileManager = FileManager.default
//        let paths = (NSSearchPathForDirectoriesInDomain((.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("documento.pdf")
        let pdfDoc = NSData(contentsOf:URL(string: strUrl)!)
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: [pdfDoc!], applicationActivities: nil)
                     activityViewController.popoverPresentationController?.sourceView=self.view
                     present(activityViewController, animated: true, completion: nil)
    }
}
