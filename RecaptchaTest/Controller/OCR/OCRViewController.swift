//
//  OCRViewController.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 11/11/2564 BE.
//

import UIKit

class OCRViewController: UIViewController {

    @IBOutlet weak var imvRecogine: UIImageView!
    @IBOutlet weak var lblResult: UILabel!
    
    var viewModel = OCRViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        imvRecogine.image = UIImage(named: "test")
    }
    
    func bindViewModel() {
        viewModel.didSuccessCallback = { result in
            self.lblResult.text = result.joined(separator: "\n")
        }
        
        viewModel.didErrorCallback = { error in
            self.lblResult.text = error
        }
        
        viewModel.didUpdateViewCallback = {
            self.view.layer.addSublayer(self.viewModel.pathLayer!)
        }
        viewModel.procressRecognizeText()
        //viewModel.detectText(image: UIImage(named: "ID_card")!)
    }
}
