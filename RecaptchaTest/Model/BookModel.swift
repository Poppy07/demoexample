//
//  BookModel.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 27/10/2564 BE.
//

import Foundation

class Book: NSObject, NSCoding, NSSecureCoding {
    static var supportsSecureCoding: Bool = true
    var title: String
    var author: String
    var published: Int
    
    init(title: String, author: String, published: Int) {
        self.title = title
        self.author = author
        self.published = published
    }
    
    required convenience init?(coder: NSCoder) {
        guard let title = coder.decodeObject(forKey: "title") as? String,
              let author = coder.decodeObject(forKey: "author") as? String
        else { return nil }
        
        self.init(
            title: title,
            author: author,
            published: coder.decodeInteger(forKey: "published")
        )
    }

    func encode(with coder: NSCoder) {
        coder.encode(title, forKey: "title")
        coder.encode(author, forKey: "author")
        coder.encode(published, forKey: "published")
    }
}
