//
//  WeatherVC.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 1/11/2564 BE.
//

import Foundation

import UIKit
import Kingfisher

class WeatherVC: UIViewController {
    
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var tempLowLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var tempIconImageView: UIImageView!

    var viewModel = WeatherViewModel(service: WeatherService())
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bindViewModel()
    }

    func bindViewModel() {
        viewModel.fetchWeather()
        viewModel.onSuccessCallback = {
            self.updateUI()
        }
        
        viewModel.onErrorCallback = { error in
            print(error)
        }
    }
    
    func updateUI() {
        cityNameLabel.text = viewModel.city
        tempLabel.text = viewModel.temp
        descriptionLabel.text = viewModel.subtitle
        titleLabel.text = viewModel.title
        humidityLabel.text = viewModel.humidity
        windLabel.text = viewModel.wind
        tempLowLabel.text = viewModel.temp

        if let url = URL(string: viewModel.iconName) {
            tempIconImageView.kf.setImage(with: url)
        }
    }

}

