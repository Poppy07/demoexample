//
//  SslService.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 1/11/2564 BE.
//

import Foundation

class SslService: NSObject {
    private let api = ApiManager.shared
    
    func apiFetchUsers(success: @escaping ([User]) -> (),
                     fail: @escaping (String) -> ()) {
        
        let param = ["order": "desc",
                     "sort": "reputation",
                     "site": "stackoverflow"].toParameterString()
        api.request(path: .TESTPOLICE, body: nil, header: nil, param: param, method: .get,
                    type: UserResponse.self) { (response) -> (Void) in
            guard let response = response else { return fail("error") }
            success(response.users)
        } onFailure: { (error) in
            fail(error)
        }
    }
}
