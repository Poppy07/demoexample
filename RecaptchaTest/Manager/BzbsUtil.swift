//
//  BzbsUtil.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 28/10/2564 BE.
//

import Foundation
import UIKit
import LocalAuthentication
import AVFoundation

struct Certificates {
  static let certificate =
    Certificates.certificate(filename: "stackexchange.com")
  
  private static func certificate(filename: String) -> SecCertificate {
    let filePath = Bundle.main.path(forResource: filename, ofType: "der")!
    let data = try! Data(contentsOf: URL(fileURLWithPath: filePath))
    let certificate = SecCertificateCreateWithData(nil, data as CFData)!
    
    return certificate
  }
}

public func delay(_ afterDelay: Double = 0.01, callBack: @escaping () -> Void) {
    let delay = afterDelay * Double(NSEC_PER_SEC)
    let time = DispatchTime.now() + Double(Int64(delay)) / Double(NSEC_PER_SEC)
    DispatchQueue.main.asyncAfter(deadline: time, execute: {
        callBack()
    })
}

func jsonWith(item: Any) -> String {
    if item is [String:Any] || item is [[String:Any]] {
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: item, options: [.prettyPrinted])
            guard let jsonString = String(data: jsonData, encoding: String.Encoding.utf8) else {
                print("Can't create string with data.")
                return ""
            }
            return jsonString
        } catch let parseError {
            print("json serialization error: \(parseError)")
            return ""
        }
    } else {
        print("json serialization error: JSON wrong format")
        return ""
    }
}

func itemWith(json: String) -> Any {
    do {
        let jsonData = json.data(using: .utf8)
        let jsonObj = try JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as Any
        return jsonObj
    } catch let parseError {
        print("json serialization error: \(parseError)")
        return ""
    }
}

func pprint(item: Any) {
    if let item = item as? Encodable {
        do {
            try pprint(item: item.asDictionary())
        } catch let error {
            print(error.localizedDescription)
        }
    } else {
        let result = jsonWith(item: item)
        if result.count > 0 {
            print(result)
        } else {
            print(item)
        }
    }
}

var biometryType: LocalAuthentication.LABiometryType {
    let context = LAContext()
    var error: NSError?
    let _ = context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error)
    if #available(iOS 11.0, *) {
        return context.biometryType
    }
    else {
        return .none
    }
}

func authenticationWithTouchID(callback: @escaping ((String) -> Void)) {
    let localAuthenticationContext = LAContext()
    localAuthenticationContext.localizedFallbackTitle = "Please use your passcode"
    
    var authorizationError: NSError?
    let reason = "Authentication required to access the secure data"
    
    if localAuthenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &authorizationError) {
        localAuthenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, evaluateError in
            if success {
                callback("")
            } else if let error = evaluateError {
                callback(error.localizedDescription)
            }
        }
    } else if let error = authorizationError {
        callback(error.localizedDescription)
    }
}

func authenticationWithCamera(callback: @escaping ((Bool) -> Void)) {
    switch AVCaptureDevice.authorizationStatus(for: .video) {
    case .authorized: // The user has previously granted access to the camera.
        //self.setupCaptureSession()
        callback(true)
        
    case .notDetermined: // The user has not yet been asked for camera access.
        AVCaptureDevice.requestAccess(for: .video) { granted in
            if granted {
                callback(true)
            }
        }
        
    case .denied: // The user has previously denied access.
        callback(false)
        
    case .restricted: // The user can't grant access due to restrictions.
        callback(false)
    default:
        break
    }
}
