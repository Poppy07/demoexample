//
//  Weather.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 1/11/2564 BE.
//

import Foundation

import Foundation

struct CurrentWeather: Codable {
    let cityName: String
    let wind: Wind
    let weather: Weather
    let status: [Sky]
    let sun: Sun
    
    enum CodingKeys: String, CodingKey {
        case cityName = "name"
        case wind = "wind"
        case weather = "main"
        case status = "weather"
        case sun = "sys"
    }
}

struct Weather: Codable {
    let temp: Double
    let humidity: Int
}

struct Sky: Codable {
    let title: String
    let subtitle: String
    let icon: String
    
    enum CodingKeys: String, CodingKey {
        case title = "main"
        case subtitle = "description"
        case icon = "icon"
    }
}
struct Wind: Codable {
    let speed: Double
}

struct Sun: Codable {
    let sunset: TimeInterval
    let sunrise: TimeInterval
    let country: String
}
