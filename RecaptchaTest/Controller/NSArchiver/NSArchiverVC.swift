//
//  NSArchiverVC.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 28/10/2564 BE.
//

import UIKit

class NSArchiverVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    @IBOutlet weak var lblResult: UILabel!
    
    @IBAction func pressedUnArchiver(_ sender: UIButton) {
        unArchiving()
    }
    
    let books = [
        Book(title: "Nineteen Eighty-Four: A Novel", author: "George Orwell", published: 1949),
        Book(title: "Brave New World", author: "Aldous Huxley", published: 1932),
        Book(title: "Mona Lisa Overdrive", author: "William Gibson", published: 1988),
        Book(title: "Ready Player One", author: "Ernest Cline", published: 2011),
        Book(title: "Red Rising", author: "Pierce Brown", published: 2014)
    ]
//    let books: [String: AnyObject] = ["pop": 39,
//                                      "sesdd": "test"] as [String: AnyObject]
    var bookList: [Book] = []
    
    let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("books")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        archiving()
    }
    
    func archiving() {
        print(path)
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: books, requiringSecureCoding: true)
            try data.write(to: path)
        } catch {
            print("ERROR: \(error.localizedDescription)")
        }
    }
    
    func unArchiving() {
        do {
            let data = try Data(contentsOf: path)
            if let bookList = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? [Book] {
                self.bookList = bookList
                self.tableview.reloadData()
            }
        } catch {
            print("ERROR: \(error.localizedDescription)")
        }
    }
}

extension NSArchiverVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! BookTVCell
        cell.lblName.text = bookList[indexPath.row].title
        cell.lblPublished.text = "D.C. \(bookList[indexPath.row].published)"
        return cell
    }
}



class BookTVCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblPublished: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
