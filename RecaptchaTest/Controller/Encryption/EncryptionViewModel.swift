//
//  EncryptionViewModel.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 23/11/2564 BE.
//

import Foundation
import SwiftyRSA
import CryptoKit
import CryptoSwift

class EncryptionViewModel: NSObject {
    
    var updateCallBack: (() -> Void)?
    
    //let secretKey = "L3LUwdZ0Vm"
    let secretKey = "Pwo6nR7EBjOwko6v"
    //let secretKey = "L3LUwdZ0VmtTmng8"
    var strEndcode: String = ""
    var hexString = ""
    var PUBLIC_KEY: PublicKey?
    var PPRIVATE_KEY: PrivateKey?
    var tag: String = ""

    
    override init() {
        super.init()
        generateKey()
    }
    
    func generateKey() {
        // 1. สร้าง Secret Key (Random)
        
        // 2. สร้าง PublicKey และ PrivateKey
        do {
            let keyPair = try SwiftyRSA.generateRSAKeyPair(sizeInBits: 2048)
            PUBLIC_KEY = keyPair.publicKey
            PPRIVATE_KEY = keyPair.privateKey
            
            print("-----BEGIN RSA PUBLIC KEY----- \n \(String(describing: try! PUBLIC_KEY!.base64String()) ) \n-----END RSA PUBLIC KEY-----")
            print("-----BEGIN RSA PRIVATE KEY----- \n \(String(describing: try! PPRIVATE_KEY!.base64String()) ) \n-----END RSA PRIVATE KEY-----")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func rsaEncrypt(str: String) {
        guard let publicKey = PUBLIC_KEY else { return }
        
        do{
            // 1. นำ PlainText มาเข้ารหัสด้วย Secret key กับ PublicKey
            let clear = try ClearMessage(string: str, using: .utf8)
            let encrypted = try clear.encrypted(with: publicKey, padding: .PKCS1)
            
            // 2. Base64String (Chiper) ไซเฟอร์ ที่เข้ารหัสเรียบร้อยแล้ว
            //strEndcode = encrypted.base64String
            print("RSA encryption: \(encrypted.base64String)")
            self.updateCallBack?()
        } catch {
            print( error.localizedDescription )
            print("RSA encryption failed" )
        }
    }
    
    func rsaDecrypt() {
        guard let privateKey = PPRIVATE_KEY else { return }

        do {
            let encrypted = try EncryptedMessage(base64Encoded: strEndcode)
            let clear = try encrypted.decrypted(with: privateKey, padding: .PKCS1)
            hexString = try! clear.string(encoding: .utf8)
            self.updateCallBack?()
            
        } catch {
            print(error.localizedDescription )
            print("the RSA decryption failed" )
        }
    }
    
    func aesEncrypt(_ str: String) {
        //let txt = "{\"reqDtm\":\"2021/05/2412:47:00\",\"grant_type\":\"password\",\"username\":\"10ES04\",\"password\":\"12345\"}"
//        {
//           "reqDtm":"2018/11/03 13:05:25",
//           "merchantTaxId":"1234567890123",
//           "branchNo":"201",
//           "saleName":"Supakorn Kunnavong",
//           "checkReq":{
//              "passportNo":"AB0987654",
//              "countryCode":"JPN"
//           }
//        }
        rsaEncrypt(str: secretKey)
        do {
            let key = Array(secretKey.utf8)
            let iv = Array("TOKENSERVICESC01".utf8)
            let gcm = GCM(iv: iv, mode: .combined)
            let encrypted = try AES(key: key, blockMode: gcm, padding: .noPadding).encrypt(str.bytes)
            //let tag = gcm.authenticationTag
            strEndcode = Data(encrypted).base64EncodedString()
            print("AES encryption: \(strEndcode)")
            self.updateCallBack?()
        } catch {
            // failed
            print(error.localizedDescription )
            print("the AES encryption failed" )
        }
    }
    
    func aesDecrypt() {
//        strEndcode = "6TOdAZZ7bGOj/6Em1o4wQ7HyU/NqZEJ51dMWrv/ZHWebCVGxa4AWE+TXUFkddANJzV1kwdTRwZd0PGcHcuGN2SjnunC0uzMwMoORTkxW6xvNgT9Jm1nrJCcbECRBUfRLY40nhHdxMXNcytEfItS3odw="
        do {
            guard let data = Data(base64Encoded: strEndcode) else { return }
            let key = Array(secretKey.utf8)
            let iv = Array("TOKENSERVICESC01".utf8)

            print("Key: \(key.toHexString()) \n IV: \(iv.toHexString())")

            let gcm = GCM(iv: iv, mode: .combined)
            let decrypted = try AES(key: key, blockMode: gcm, padding: .noPadding).decrypt(data.bytes)
            let result = String(bytes: decrypted, encoding: .utf8) ?? ""
            hexString = result
            self.updateCallBack?()
        } catch {
            print(error.localizedDescription )
            print("the AES decryption failed" )
        }
    }
}
