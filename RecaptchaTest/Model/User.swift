//
//  User.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 1/11/2564 BE.
//

import Foundation

struct UserResponse: Codable {
    let HasMore: Bool
    let users: [User]
    
    enum CodingKeys: String, CodingKey {
        case HasMore = "has_more"
        case users = "items"
    }
}

struct User: Codable {
    let name: String
    let profileImage: String
    
    enum CodingKeys: String, CodingKey {
        case name = "display_name"
        case profileImage = "profile_image"
    }
}
