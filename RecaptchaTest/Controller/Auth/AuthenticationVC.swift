//
//  AuthenticationVC.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 2/11/2564 BE.
//

import UIKit

class AuthenticationVC: UIViewController {

    @IBOutlet weak var hiddenPinTextField: UITextField!
    @IBOutlet weak var pinStackView: UIStackView!
    @IBOutlet weak var bioAuthButton: UIButton!
    @IBOutlet weak var faceIdImage: UIImageView!
    @IBOutlet weak var forgetPinButton: UIButton!
    
    let viewModel = AuthenticationVM()
    override func viewDidLoad() {
        super.viewDidLoad()
        hiddenPinTextField.delegate = self
        hiddenPinTextField.autocorrectionType = .no
        hiddenPinTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        hiddenPinTextField.inputAccessoryView = .none
        hiddenPinTextField.isAccessibilityElement = false
        resetPin()
        
        switch biometryType {
        case .none:
            bioAuthButton.isHidden = true
        case .touchID:
            bioAuthButton.setTitle("Touch ID", for: UIControl.State())
            bioAuthButton.isHidden = false
            viewModel.getAuthenticationWithTouchID()
        case .faceID:
            bioAuthButton.setTitle("FACE ID", for: UIControl.State())
            bioAuthButton.isHidden = false
            faceIdImage.isHidden = false
            viewModel.getAuthenticationWithTouchID()
        @unknown default:
            bioAuthButton.isHidden = true
        }
    }
    
    @IBAction func pressBiometricAuthen(_ sender: UIButton) {
        viewModel.getAuthenticationWithTouchID()
    }
    
    func resetPin() {
//        hideHud()
//        if !Keys.isUseBiometry.value! {
//            hiddenPinTextField.becomeFirstResponder()
//        }
        
        hiddenPinTextField.becomeFirstResponder()
        
        if viewModel.isBioAuthFail {
            hiddenPinTextField.becomeFirstResponder()
        }
        
        hiddenPinTextField.text = ""
        updatePinView(pin: "")
    }
    
    func updatePinView(pin: String) {
        for i in 0..<pinStackView.arrangedSubviews.count {
            if let imageView = pinStackView.arrangedSubviews[i] as? UIImageView {
                if i < pin.count {
                    imageView.image = UIImage(named: "pincode-filled")
                } else {
                    imageView.image = UIImage(named: "pincode-empty")
                }
            }
        }
    }
}

extension AuthenticationVC: UITextFieldDelegate {
    @objc func textFieldDidChange(sender: UITextField) {
        if let pin = hiddenPinTextField.text {
            updatePinView(pin: pin)
            if pin.count == 6 {
                if viewModel.pincodeState != .create {
                    self.view.endEditing(true)
                }
                viewModel.verifyPin(pin: pin)
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newString = textField.newReplacementString(replace: string, range: range)
        if newString.count > 6 {
            return false
        }
        
        return true
    }
}
