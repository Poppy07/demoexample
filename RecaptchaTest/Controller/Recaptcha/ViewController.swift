//
//  ViewController.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 21/9/2564 BE.
//

import UIKit
import ReCaptcha
import WebKit
import Firebase
import FirebaseAnalytics

class ViewController: UIViewController {

    @IBOutlet weak var lblToken: UILabel!
    
    @IBAction func pressedValidate(_ sender: UIButton) {
        print("recaptcha Validate")
 //       self.removeWebview(isHidden: false)
        validate()
//        setGAEvent()
    }
    
    @IBAction func pressedStop(_ sender: UIButton) {
        print("recaptcha Stop")
        removeWebview()
    }
    
    @IBAction func pressedReset(_ sender: UIButton) {
        print("recaptcha Reset")
//        recaptcha?.reset()
    }
        
    var recaptcha = try? ReCaptcha()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupRecaptcha()
       // Analytics.logEvent(AnalyticsEventScreenView, parameters: [AnalyticsParameterScreenName : "recaptcha_screen"])
    }

    func setupRecaptcha() {
        recaptcha = try? ReCaptcha()
        recaptcha?.configureWebView { [weak self] webview in
            webview.frame = self?.view.bounds ?? .zero
            webview.backgroundColor = UIColor.gray.withAlphaComponent(0.3)
            webview.isOpaque = false
        }
    }
    
    func validate() {
        recaptcha?.validate(on: view, resetOnError: false) { [weak self] (result: ReCaptchaResult) in
            switch result {
            case .error(let error):
                print("error recaptcha \(error.description)")
            case .token(let token):
                print("recaptchaToken: \(token)")
                self?.lblToken.text = token
                self?.removeWebview()
                break
            }
        }
    }
    
    func removeWebview() {
        recaptcha = nil
        if let webviewPage = self.view.subviews.filter({$0 is WKWebView}).first {
            webviewPage.removeFromSuperview()
            self.setupRecaptcha()
        }
    }
    
    func setGAEvent() {
        Analytics.logEvent("recaptcha_page", parameters: [
            "event" : "click_validate_recptcha"  as AnyObject,
            "label" : "test pop 1234" as AnyObject,
            ])
    }
}
