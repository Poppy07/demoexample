//
//  WeatherService.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 1/11/2564 BE.
//

import Foundation

class WeatherService: NSObject {

    private let api = ApiManager.shared
    private let apiKey = "a4bb0a1a6abdb30931ec697db4057199"
    let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("books")
    
    func apiFetchWeather(success: @escaping (CurrentWeather) -> (),
                     fail: @escaping (String) -> ()) {
        let param = ["q": "bangkok",
                     "appid": apiKey].toParameterString()
        api.request(path: .getWeather, body: nil, header: nil, param: param, method: .get,
                    type: CurrentWeather.self) { (response) -> (Void) in
            guard let response = response else { return fail("error") }
            //self.archiving(type: response)
            success(response)
        } onFailure: { (error) in
            fail(error)
        }
    }
    
//    func archiving<T: Codable>(type: T.Type) {
//        do {
//            let data = try NSKeyedArchiver.archivedData(withRootObject: type, requiringSecureCoding: true)
//            try data.write(to: path)
//            self.unArchiving(type: [type])
//        } catch {
//            print("ERROR: \(error.localizedDescription)")
//        }
//    }
//
//    func unArchiving<T: Codable>(type: T.Type) {
//        do {
//            let data = try Data(contentsOf: path)
//            if let bookList = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(data) as? type {
//                pprint(item: bookList)
//            }
//        } catch {
//            print("ERROR: \(error.localizedDescription)")
//        }
//    }
}
