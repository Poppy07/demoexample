//
//  ApiConstant.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 28/10/2564 BE.
//

import Foundation

enum ApiConstant: String {
    
    case TESTPOLICE          = "users"
    case getWeather          = "weather"
}
