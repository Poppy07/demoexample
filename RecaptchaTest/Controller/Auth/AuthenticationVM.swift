//
//  AuthenticationVM.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 2/11/2564 BE.
//

import Foundation

enum PincodeState {
    case create
    case confirm
    case verify
//    case change
//    case newPin
//    case confirmNewPin
}


class AuthenticationVM: NSObject {
    var createPinSuccessCallback: (() -> Void)?
    var verifyPinSuccessCallback: (() -> Void)?
    var verifyBioFailCallback: (() -> Void)?
    var updatePinStateCallback: (() -> Void)?
    
    var pincodeState = PincodeState.create
    var pincode = ""
    var isBioAuthFail = false
    
    func verifyPin(pin: String) {
        switch pincodeState {
        case .create:
            pincode = pin
            pincodeState = .confirm
            updatePinStateCallback?()
        case .confirm:
            if pin == pincode {
                //apiCreatePin(pincode: pin)
            } else {
                pincodeState = .create
            }
        case .verify:
            break
            //apiVerifyPin(pincode: pin)
        }
    }
    
    func getAuthenticationWithTouchID() {
        authenticationWithTouchID { (error) in
            if error.isEmpty {
                DispatchQueue.main.async {
                    //self.apiVerifyPinByBioAuth()
                }
            } else {
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self.isBioAuthFail = true
                    self.verifyBioFailCallback?()
                }
            }
        }
    }
}
