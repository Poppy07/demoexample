//
//  MenuVC.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 28/10/2564 BE.
//

import UIKit
import SkeletonView

enum Menu: String {
    case Recaptcha  = "Recaptcha"
    case NSArch     = "NSArching"
    case Ssl        = "SSL Service police"
    case Encryption = "Encryption"
    case Caching    = "Caching"
    case Keychain   = "Keychain"
    case Weather    = "Weather API"
    case Auth       = "Authentication"
    case Web        = "PDF"
    case FaceRecognition = "Face Recognition"
    case OCR        = "OCR"
}

class MenuVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    
    var menuList: [Menu] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self
        tableview.rowHeight = 50
        tableview.estimatedRowHeight = 50
        
        tableview.isSkeletonable = true
        tableview.showGradientSkeleton()
        tableview.showSkeleton(usingColor: .red, transition: .crossDissolve(0.25))
        
        delay(1.0) {
            self.menuList = [.Recaptcha, .Encryption, .NSArch, .Weather, .Ssl, .Auth, .Web, .FaceRecognition, .OCR]
            self.tableview.stopSkeletonAnimation()
            self.view.hideSkeleton()
            self.tableview.reloadData()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
}


extension MenuVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")!
        cell.textLabel?.text = menuList[indexPath.row].rawValue
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch menuList[indexPath.row] {
        case .Recaptcha:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "ViewController") as? ViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        case .NSArch:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NSArchiverVC") as? NSArchiverVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .Ssl:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "SslVC") as? SslVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .Encryption:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "EncryptionVC") as? EncryptionVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .Weather:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WeatherVC") as? WeatherVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .Auth:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "AuthenticationVC") as? AuthenticationVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .Web:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebViewVC") as? WebViewVC
            self.navigationController?.pushViewController(vc!, animated: true)
        case .OCR:
            let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "OCRView") as? OCRViewController
            self.navigationController?.pushViewController(vc!, animated: true)
        case .FaceRecognition:
            authenticationWithCamera { permission in
                if permission {
                    delay {
                        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "FaceRecoginiteViewController") as? FaceRecoginiteViewController
                        self.navigationController?.pushViewController(vc!, animated: true)
                    }
                } else {
                    print("Permission error")
                }
            }
        default:
            break
        }
    }
}

extension MenuVC: SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "cell"
    }
}
