//
//  EncryptionVC.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 28/10/2564 BE.
//

import UIKit
import CryptoKit
import CryptoSwift

class EncryptionVC: UIViewController {
    
    @IBOutlet weak var txtInput: UITextField!
    @IBOutlet weak var lblEncode: UILabel!
    @IBOutlet weak var lblDecode: UILabel!
    
    @IBAction func pressedDecryption(_ sender: UIButton) {
        print("Decryption Code")
        //viewModel.rsaDecrypt()
        txtInput.text = "{\"reqDtm\":\"2021/05/2412:47:00\",\"grant_type\":\"password\",\"username\":\"10ES04\",\"password\":\"12345\"}"
        viewModel.aesDecrypt()
    }
    
    let viewModel = EncryptionViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtInput.delegate = self
        bindViewModel()
    }
    
    func bindViewModel() {
        viewModel.updateCallBack = {
            self.lblEncode.text = self.viewModel.strEndcode
            self.lblDecode.text = self.viewModel.hexString
        }
    }
}

extension EncryptionVC: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        //viewModel.rsaEncrypt(str: text)
        viewModel.aesEncrypt(text)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}
