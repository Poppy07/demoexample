//
//  AfterSplashViewController.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 29/10/2564 BE.
//

import UIKit
import Lottie

class AfterSplashViewController: UIViewController {

    @IBOutlet var animationView: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        animationView.contentMode = .scaleAspectFit
        animationView.loopMode = .loop
        animationView.animationSpeed = 2.0
        animationView.play()
        
        delay(5.0) {
            let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let nv = storyboard.instantiateViewController(withIdentifier: "Nav") as! UINavigationController
            UIApplication.shared.windows.first?.rootViewController = nv
        }
    }
}
