//
//  UtilitesManager.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 1/11/2564 BE.
//

import Foundation
import Alamofire

extension Encodable {
    func asDictionary() throws -> [String: Any] {
        let data = try JSONEncoder().encode(self)
        guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            throw NSError()
        }
        return dictionary
    }
}

extension Dictionary {
    mutating func merge(dict: [Key: Value]){
        for (k, v) in dict {
            updateValue(v, forKey: k)
        }
    }
    
    func toParameterString() -> String {
        var param = ""
        for key in self.keys {
            if let value = self[key] as? String {
                param = param + String(describing: key) + "=" + value + "&"
            }
        }
        if let _ = param.popLast() { }
        return param
    }
    
    func mapDataDicToMultipleFormData() -> MultipartFormData {
        let formData = MultipartFormData()
        self.forEach({
            let value = "\($0.value)"
            let key = "\($0.key)"
            formData.append(value.data(using: String.Encoding.utf8, allowLossyConversion: false)!, withName: key)
        })
        
        return formData
    }
}

extension UITextField {
    func newReplacementString(replace:String, range:NSRange) -> String {
        if let tempRange = Range(range, in: self.text!) {
            let replaceString = self.text!.replacingCharacters(in: tempRange, with: replace)
            return replaceString
        }
        return ""
    }
}
