//
//  SslVC.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 28/10/2564 BE.
//

import UIKit
import SkeletonView

class SslVC: UIViewController {

    @IBOutlet weak var tableview: UITableView!
    var users: [User] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableview.dataSource = self
        tableview.rowHeight = 120
        tableview.estimatedRowHeight = 120
        
        tableview.isSkeletonable = true
        tableview.showAnimatedGradientSkeleton()
        tableview.showSkeleton(usingColor: .green, transition: .crossDissolve(0.25))
        
        fetchUsers()
    }
    
    func fetchUsers() {
        SslService().apiFetchUsers { users in
            self.users = users
            self.tableview.stopSkeletonAnimation()
            self.view.hideSkeleton()
            self.tableview.reloadData()
        } fail: { error in
            print(error)
        }
    }
}

extension SslVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! UserTVCell
        let user = users[indexPath.row]
        cell.lblName.text = user.name
        if let url = URL(string: user.profileImage) {
            cell.imvProfile.kf.setImage(with: url)
        }
        return cell
    }
}

extension SslVC: SkeletonTableViewDataSource {
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "cell"
    }
}



class UserTVCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var imvProfile: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        lblName.lastLineFillPercent = 70
        lblName.linesCornerRadius = 5
    }
}
