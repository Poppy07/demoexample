//
//  ApiManager.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 28/10/2564 BE.
//

import Foundation
import Alamofire
import SystemConfiguration
import UIKit

open class ApiManager: NSObject {

    static let shared = ApiManager()
    private var manager : Alamofire.Session?
    private var baseUrlNoSSL = "https://api.openweathermap.org/data/2.5/"
    private var baseUrl = "https://api.stackexchange.com/"

    func getManager() -> Alamofire.Session {
        if let m = self.manager {
            return m
        } else {
            let defaultManager: Alamofire.Session = {
                let serverTrustManager = ServerTrustManager(evaluators: [
//                    "api.stackexchange.com": PinnedCertificatesTrustEvaluator(certificates: [
//                        Certificates.certificate]
//                      ),
                    "api.stackexchange.com": DisabledTrustEvaluator(),
                    "api.openweathermap.org": DisabledTrustEvaluator()
                ])

                let configuration = URLSessionConfiguration.af.default
                configuration.headers = .default
                configuration.timeoutIntervalForRequest = 30

                let manager = Session(configuration: configuration, serverTrustManager: serverTrustManager)

                return manager
            } ()

            self.manager = defaultManager
            return self.manager!
        }
    }

    func connectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout<sockaddr_in>.size)
        zeroAddress.sin_family = sa_family_t(AF_INET)

        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                SCNetworkReachabilityCreateWithAddress(nil, $0)
            }
        }) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }
    
    func request<T: Decodable>(path: ApiConstant,
                 body: [String:Any]?,
                 header: [String: Any]?,
                 param: String = "",
                 method: HTTPMethod = .post,
                 type: T.Type,
                 onSuccess: @escaping (_ result: T?) -> (),
                 onFailure: @escaping ((String) -> Void)) {
        if connectedToNetwork() {
            var urlString = "\(baseUrlNoSSL)\(path.rawValue)"
            
            if path == .TESTPOLICE {
                urlString = "\(baseUrl)\(path.rawValue)"
            }
            
            if param != "" {
                urlString = "\(urlString)?\(param)"
            }
            
            if method == .get {
                urlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
            }
            
            if let url = URL(string: urlString) {
                var request = URLRequest(url: url)
                request.method = method
                request.headers = self.getCommonHeader(headers: header)
                
                print("=============== Start: Call Service ===============")
                print("URL :", url)
                print("Method : \(method.rawValue)")
                
                if body != nil{
                    request.httpBody = try! JSONSerialization.data(withJSONObject: body!)
                    print("********** body **********")
                    pprint(item: body!)
                }
                
                
            getManager().request(request).validate(statusCode: 200...500).responseDecodable(of: type) { (response) in
                    //debugPrint(request)
                    switch response.result {
                    case .success (let data):
                        print("********** response **********")
                        pprint(item: data)
                        
                        if response.response!.statusCode < 300 {
                            onSuccess(data)
                        } else if response.response!.statusCode == 401 {
                            //TODO SOMETHING
                            break
                        } else {
//                            if let responseValue = data as? GeneralModel {
//                                onFailure(responseValue.message)
//                            } else
                            if let responseValue = data as? Encodable {
                                do {
                                    let value = try responseValue.asDictionary()
                                    onFailure(value["message"] as? String ?? "")
                                } catch let error {
                                    onFailure(error.localizedDescription)
                                }
                            } else {
                                onFailure("Something wrong")
                            }
                        }
                        break
                    case .failure(let error):
                        onFailure(error.localizedDescription)
                        print(error.localizedDescription)
                        print(error)
                        let message: String
                        let isServerTrustEvaluationError =
                                error.asAFError?.isServerTrustEvaluationError ?? false
                        if isServerTrustEvaluationError {
                            message = "Certificate Pinning Error"
                        } else {
                            message = error.localizedDescription
                        }
                        print(message)
                        break
                    }
                    print("=============== End: Call Service ===============")
                }
            } else {
                onFailure("Please connect to internet while using the application")
            }
        } else {
            onFailure("Please connect to internet while using the application")
        }
    }
    
    func getCommonHeader(headers: [String: Any]?) -> HTTPHeaders {
        var commonParam: HTTPHeaders = [
            "Content-Type": "application/json"
//            "token": "tyduiodjdjdkdk",
//            "device": "",
//            "language" : "1054",
//            "longitude": "0",
//            "latitude": "0",
//            "app_id": "1234567899"
        ]
        if headers != nil {
            headers!.forEach({ (key, value) in
                commonParam.add(name: key, value: "\(value)")
            })
        }
        return commonParam
    }
}
