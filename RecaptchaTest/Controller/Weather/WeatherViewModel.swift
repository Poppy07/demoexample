//
//  WeatherViewModel.swift
//  RecaptchaTest
//
//  Created by Saowalak Rungrat on 1/11/2564 BE.
//

import Foundation
import UIKit
import Kingfisher

class WeatherViewModel: NSObject {
    
    private var service: WeatherService!
    
    var weather: CurrentWeather!
    
    var temp: String {
        let temp = weather.weather.temp - 273.15
        return String(format: "%0.1f", temp).appending("°C")
    }
    
    var city: String {
        return weather.cityName
    }
    
    var subtitle: String {
        return weather.status.compactMap{($0.subtitle)}.first ?? ""
    }
    
    var title: String {
        return weather.status.compactMap{($0.title)}.first ?? ""
    }
    
    var iconName: String {
        let name = weather.status.compactMap{($0.icon)}.first ?? ""
        return "http://openweathermap.org/img/wn/\(name)@2x.png"
    }
    
    var wind: String {
        return  String(weather.wind.speed) + "m/s"
    }
    
    var humidity: String {
        return String(weather.weather.humidity) + "%"
    }
    
    var onErrorCallback: ((String) -> Void)?
    var onSuccessCallback: (() -> Void)?
    
    // MARK: - Initializers
    init(service: WeatherService) {
        super.init()
        self.service = service
    }
    
    func fetchWeather() {
        service.apiFetchWeather { (currentWeather) in
            self.weather = currentWeather
            self.onSuccessCallback?()
        } fail: { (error) in
            self.onErrorCallback?(error)
        }
    }
}
